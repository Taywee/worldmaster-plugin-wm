use std::{convert::TryFrom, ops::{Deref, DerefMut}, str, string::FromUtf8Error};
use serde::{Serialize, Deserialize};

/// A simple metadata structure.
#[derive(Debug, Serialize, Deserialize)]
pub struct Metadata {
    /// The short id used as a prefix for all types in this plugin
    id: String,

    /// The proper name for this plugin
    name: String,

    /// A user-friendly description of this plugin
    description: String,

    /// The semver version of the plugin
    version: String,
}

#[derive(Debug, Hash, PartialOrd, PartialEq, Ord, Eq)]
pub struct Buffer(Vec<u8>);

impl From<String> for Buffer {
    fn from(value: String) -> Self {
        Buffer(value.into_bytes())
    }
}

impl From<&str> for Buffer {
    fn from(value: &str) -> Self {
        Buffer(value.as_bytes().to_vec())
    }
}

impl From<Vec<u8>> for Buffer {
    fn from(value: Vec<u8>) -> Self {
        Buffer(value)
    }
}

impl From<&[u8]> for Buffer {
    fn from(value: &[u8]) -> Self {
        Buffer(value.to_vec())
    }
}

impl TryFrom<Buffer> for String {
    type Error = FromUtf8Error;

    fn try_from(value: Buffer) -> Result<Self, Self::Error> {
        String::from_utf8(value.0)
    }
}

impl Deref for Buffer {
    type Target = Vec<u8>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Buffer {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

#[no_mangle]
pub extern "C" fn buffer_new(size: usize) -> Box<Buffer> {
    let mut vec = Vec::with_capacity(size);
    vec.resize(size, 0);
    Box::new(Buffer(vec))
}

#[no_mangle]
pub extern "C" fn buffer_free(_buffer: Box<Buffer>) {
}

#[no_mangle]
pub extern "C" fn buffer_len(buffer: &Buffer) -> usize {
    buffer.len()
}

#[no_mangle]
pub extern "C" fn buffer_const_ptr(buffer: &Buffer) -> *const u8 {
    buffer.as_ptr()
}

#[no_mangle]
pub extern "C" fn buffer_mut_ptr(buffer: &mut Buffer) -> *mut u8 {
    buffer.as_mut_ptr()
}

#[no_mangle]
pub extern "C" fn metadata() -> Box<Buffer> {
    let metadata = Metadata {
        id: "wm".into(),
        name: "worldmaster".into(),
        description: "The core worldmaster plugin.".into(),
        version: "0.0.1".into(),
    };
    let metadata = serde_json::to_vec(&metadata).unwrap();

    Box::new(Buffer(metadata))
}

/// The wiki types for this plugin
#[derive(Debug, Serialize, Deserialize)]
pub enum WikiType {
    #[serde(rename = "wm.title")]
    Title(String),
}

/// Convert the wiki type to html
#[no_mangle]
pub extern "C" fn wiki_type_to_html(buffer: &Buffer) -> Box<Buffer> {
    let wiki_type: WikiType = serde_json::from_slice(&buffer).unwrap();
    match wiki_type {
        WikiType::Title(title) => Box::new(format!("<h1>{}</h1>", title).into()),
    }
}
